﻿using RH.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Dao
{
    class FuncionarioDao : Idao<Funcionario>
    {
        // atributos
        // conexão com o banco de dados
        private SqlConnection connection;

        // instrução sql
        private string sql = null;

        // mensagem do messagebox
        private string msg = null;

        // titulo do messagebox
        private string titulo = null;

        // construtor
        public FuncionarioDao()
        {
            // cria uma conexão com o banco de dados
            connection = new ConnectionFactory().GetConnection();
        }

        // metodos
        public List<Funcionario> Consultar()
        {
            // comando sql de consulta
            sql = "SELECT * FROM Funcionario";

            // lista de funcionarios cadastrados
            List<Funcionario> funcionarios = new List<Funcionario>();

            try
            {
                // abre a conexão com o banco de dado
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // cria um leitor de dados
                SqlDataReader leitor = cmd.ExecuteReader();

                while (leitor.Read())
                {

                    Funcionario funcionario = new Funcionario();

                    funcionario.Id = (long)leitor["IDFuncionario"];
                    funcionario.Nome = leitor["Nome"].ToString();
                    funcionario.Cpf = leitor["Cpf"].ToString();
                    funcionario.Rg = leitor["Rg"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();

                    // adiciona o funcionário a lista de funcionários
                    funcionarios.Add(funcionario);

                } // fim do while



            }
            catch (SqlException ex)
            {

                msg = "Erro ao consultar os funcionários cadastrados: " + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                connection.Close();
            }
            return funcionarios;


        }

        public void Excluir(Funcionario funcionario)
        {
            throw new NotImplementedException();
        }

        public void Salvar(Funcionario funcionario)
        {
            try
            {
                sql = "INSERT INTO Funcionario(Nome, Cpf, Rg, Email, Telefone) VALUES(@Nome, @Cpf, @Rg, @Email, @Telefone )";

                // abre uma conexão com o banco de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@Nome", funcionario.Nome);
                cmd.Parameters.AddWithValue("@Cpf", funcionario.Cpf);
                cmd.Parameters.AddWithValue("@Rg", funcionario.Rg);
                cmd.Parameters.AddWithValue("@Email", funcionario.Email);
                cmd.Parameters.AddWithValue("@Telefone", funcionario.Telefone);

                // executa o INSERT
                cmd.ExecuteNonQuery();

                msg = "Funcionario " + funcionario.Nome + " salvo com sucesso !";
                titulo = "Sucesso...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                msg = "Erro ao salvar o funcionário !";
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }



        }
    }

}
